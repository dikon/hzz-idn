#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creator:          D. Herre
GitLab:      dikon/hzz-idn

Created:        2020-06-27
Last Modified:  2020-07-08
"""

import re
from librair import schemas
from librair import services

printed = schemas.json.reader("data/zdb-printed.json")
printed_data = []
printed_years = []
printed_titles = []
printed_publisher = []
counter = 0

DC_TITLE = "http://purl.org/dc/elements/1.1/title"
DC_ISSUED = "http://purl.org/dc/terms/issued"
DC_PUBLISHER = "http://purl.org/dc/elements/1.1/publisher"

for zdb_nr in printed:
    if zdb_nr is not None:
        item_data = services.zdb.request(zdb_nr, schema="jsonld")
        printed_data.append(item_data)
        item_title = "!NO DATA!"
        item_years = "!NO DATA!"
        item_publisher = "!NO DATA!"
        for sec in item_data:
            if DC_TITLE in sec:
                item_title = " ".join(
                    [t['@value'] for t in sec[DC_TITLE]]
                                      ).strip()
                item_title = re.sub("\\s+", " ", item_title)
                counter += 1
            if DC_ISSUED in sec:
                item_years = " ".join(
                    [y['@value']for y in sec[DC_ISSUED]]
                                      ).strip()
            if DC_PUBLISHER in sec:
                item_publisher = " ".join(
                    [p['@value'] for p in sec[DC_PUBLISHER]]
                                          ).strip()
        printed_titles.append(item_title)
        printed_years.append(item_years)
        printed_publisher.append(item_publisher)
    else:
        printed_data.append([])
        printed_years.append("NO DATA!!")
        printed_titles.append("NO DATA!!")
        printed_publisher.append("NO DATA!!")

if counter > 0:
    print("PRINTED SERIAL WORKS")
    print("")
    print("found", str(counter),
          "from", str(len(printed_titles)),
          "items with publication place",
          "\"Halae\", \"Halis\", \"Halle\", \"Halle,S.\" or \"Halle,Saale\"",
          "in ZDB!")
    print("")
    for i, item_title in enumerate(printed_titles):
        if item_title == "NO DATA!!":
            continue
        num = str(i+1)
        if len(num) == 1:
            num = "  " + num
        elif len(num) == 2:
            num = " " + num
        print("HZZ", num, " ",
              printed_years[i], " ",
              item_title, " ",
              printed_publisher[i])
