#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creator:          D. Herre
GitLab:      dikon/hzz-idn

Created:        2020-06-30
Last Modified:  2020-07-08
"""

from librair import schemas
from librair import interfaces

printed = schemas.json.reader("data/gvk-printed.json")
gvk = interfaces.unapi.Client("http://unapi.k10plus.de", "gvk", "ppn")

printed_data = []
printed_years = []
printed_titles = []
printed_publisher = []
counter = 0
max_year = -1

FORMAT = "jsmf-json"

for ppn in printed:
    if ppn is not None:
        counter += 1
        item_data = gvk.request(ppn, FORMAT)
        printed_data.append(item_data)
        item_title = "!NO DATA!"
        item_years = "!NO DATA!"
        item_publisher = "!NO DATA!"
        if 'title' in item_data:
            if type(item_data["title"]) == list:   # alternative titles present
                item_title = item_data["title"][0]
            else:
                item_title = item_data["title"]
        if 'publisher' in item_data:
            item_publisher = item_data["publisher"]
        if 'year' in item_data:
            item_years = item_data["year"]
            if len(item_years) > max_year:
                max_year = len(item_years)
        printed_titles.append(item_title)
        printed_years.append(item_years)
        printed_publisher.append(item_publisher)
    else:
        printed_data.append({})
        printed_years.append("NO DATA!!")
        printed_titles.append("NO DATA!!")
        printed_publisher.append("NO DATA!!")


if counter > 0:
    print("PRINTED SERIAL WORKS")
    print("")
    print("found", str(counter),
          "from", str(len(printed_titles)),
          "items with publication place",
          "\"Halle\"",
          "in GVK!")
    print("")
    for i, item_title in enumerate(printed_titles):
        if item_title == "NO DATA!!":
            continue
        num = str(i+1)
        if len(num) == 1:
            num = "  " + num
        elif len(num) == 2:
            num = " " + num
        item_years = printed_years[i]
        if len(item_years) < max_year:
            item_years += (max_year-len(item_years)) * " "
        print("HZZ", num, " ",
              item_years, " ",
              item_title, " ",
              printed_publisher[i])
