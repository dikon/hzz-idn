# Identifikationsnummern der halleschen Zeitungen und Zeitschriften

Bereits Mitte der 1990er-Jahre wurden am IZEA – damals unter der Schirrmherrschaft von [Holger Böning](https://de.wikipedia.org/wiki/Holger_B%C3%B6ning) – erste Versuche unternommen, die zu Zeiten der Aufklärung enstehende Presselandschaft Halles in Form einer Bibliographie zusammenzutragen. Geraume Zeit später, zu Beginn der 2010er-Jahre, wurden diese Vorarbeiten aufgegriffen, fortgeführt und schließlich jüngst als Teil der _Schriften zum Bibliotheks- und Büchereiwesen in Sachsen-Anhalt_ digital veröffentlicht:

> Anne Purschwitz, _Die halleschen Zeitungen und Zeitschriften im Zeitalter der Aufklärung (1688–1815) – Bibliographisches Verzeichnis_. Halle/Saale: Universitäts- und Landesbibliothek Sachsen-Anhalt, 2018. DOI: [10.25673/13487](https://doi.org/10.25673/13487).

In dieser Publikation sind zu den einzelnen Titeln jeweils die bibliothekarischen Kataloge vermerkt, in denen sie nachweisbar sind. Der Fokus liegt dabei vorrangig auf der [Zeitschriftendatenbank](https://zdb-katalog.de/) (ZDB) und dem [Gemeinsamen Verbundkatalog](https://gso.gbv.de/DB=2.1/) (GVK).

Auf Basis einer umfangreichen [Bereitstellung von Daten aus diesen Katalogen](./../../../../hzz-pica) wurden hier nun die bis dato nicht miterschlossenen Identifikationsnummern der korrespondierenden Katalogisate in einzelnen [JSON-Dateien](./data/) verzeichnet. Dabei wurde für die Datensätze aus der ZDB auf die ZDB-Nummer und für die Datensätze aus dem GVK auf die Pica-Produktionsnummer (PPN) zurückgegriffen. Bei ihnen handelt es sich um bibliographische Normnummern, mit denen die Katalogisate eindeutig identifiziert, zitiert und zudem auch über entsprechende Schnittstellen automatisiert abgefragt werden können.
